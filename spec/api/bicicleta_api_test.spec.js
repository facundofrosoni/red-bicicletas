var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta.js');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:5000/api/bicicletas";


describe('Bicicleta API', () => {
    beforeAll(function(done){
        mongoose.disconnect();
        done();
    });
    beforeEach(function(done) {
        var mongodb = 'mongodb://localhost/testdb';
        mongoose.connect(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('\nYa estamos conectados a la base de datos de prueba!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "Rojo", "modelo": "Urbana", "lat": -26.812386, "lng": -65.263015}';
            request.post({
                headers:    headers,
                url:        base_url+'/create',
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("Rojo");
                expect(bici.ubicacion[0]).toBe(-26.812386);
                expect(bici.ubicacion[1]).toBe(-65.263015);
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "Rojo", "modelo": "Urbana", "lat": -26.812386, "lng": -65.263015}';
            request.post({
                headers:    headers,
                url:        base_url+'/create',
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
            var biciABorrar = '{"code": 10}';
            request.delete({
                headers:    headers,
                url:        base_url+'/delete',
                body:       biciABorrar
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });
    
    describe('UPDATE BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "Rojo", "modelo": "Urbana", "lat": -26.812386, "lng": -65.263015}';
            request.post({
                headers:    headers,
                url:        base_url+'/create',
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
            let _id = 0;
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                _id = result.bicicletas[0]._id;
                console.log("CÓDIGO: " + _id);
                var biciAActualizar = '{"_id": "'+ _id +'", "code": 10, "color": "Verde", "modelo": "Carrera", "lat": -26, "lng": -65}';
                request.post({
                    headers:    headers,
                    url:        base_url+'/update',
                    body:       biciAActualizar
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                });
                request.get(base_url, function(error, response, body) {
                    var result = JSON.parse(body);
                    expect(response.statusCode).toBe(200);
                    expect(result.bicicletas[0].color).toBe("Verde");
                    expect(result.bicicletas[0].modelo).toBe("Carrera");
                    expect(result.bicicletas[0].ubicacion[0]).toBe(-26);
                    expect(result.bicicletas[0].ubicacion[1]).toBe(-65);
                    done();
                });
            });
        });
    });
});



// describe('Bicicleta API', () => {
//     describe('GET BICICLETAS /', () => {
//         it('Status 200', () => {
//             expect(Bicicleta.allBicis.length).toBe(0);
            
//             var a = new Bicicleta(1, 'rojo', 'urbana', [-26.812386, -65.263015]);

//             Bicicleta.add(a); 

//             request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
//                 expect(response.statusCode).toBe(200);
//             });
//         });
//     });
    
// });

// describe('Bicicleta API', () => {
//     describe('POST BICICLETAS /create', () => {
//         it('STATUS 200', (done) => {
//             var headers = {'content-type' : 'application/json'};
//             var aBici = '{"id": 10, "color": "Rojo", "modelo": "Urbana", "lat": -34, "lng": -54}';
            // request.post({
            //     headers:    headers,
            //     url:        'http://localhost:5000/api/bicicletas/create',
            //     body:       aBici
            // }, function(error, response, body) {
            //     // console.log('error:', error); // Print the error if one occurred
            //     // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            //     // console.log('body:', body); // Print the HTML for the Google homepage.
            //     expect(response.statusCode).toBe(200);
            //     expect(Bicicleta.findById(10).color).toBe("Rojo");
            //     done();
            // });
//         });
//     });
// });

