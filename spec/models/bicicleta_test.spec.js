var Bicicleta = require("../../models/bicicleta.js");
const mongoose = require('mongoose');


describe('Testing Bicicletas', function(){
    beforeEach(function(done) {
        var mongodb = 'mongodb://localhost/testdb';
        mongoose.connect(mongodb, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('\nYa estamos conectados a la base de datos de prueba!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [-26.8, -65.2]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toEqual(-26.8);
            expect(bici.ubicacion[1]).toEqual(-65.2);
            done();
        });
    });
    
    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });
    
    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
    
                    done();    
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver una bici con código 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "Roja", modelo: "Urbana"});
                Bicicleta.add(aBici, function (err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "Verde", modelo: "Mounain-Byke"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });


});

// beforeEach(()=>{
//     Bicicleta.allBicis = [];
// });
// beforeEach(function(){
//     console.log('testeando…');
// });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacía', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add',() => {
//     it('agregamos una', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
        
//         var a = new Bicicleta(1, 'rojo', 'urbana', [-26.812386, -65.263015]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);

//     });
// });

// describe('Bicicleta.findById', () => {
//     it('debe devolver una bici con id 1', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0)
//         var aBici1 = new Bicicleta(1, 'Verde', 'Urbana', [-26.812386, -65.263015]);
//         var aBici2 = new Bicicleta(2, 'Blanco', 'Montaña', [-26.812386, -65.263050]);

//         Bicicleta.add(aBici1);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe("Verde");
//         expect(targetBici.modelo).toBe("Urbana");
//         expect(targetBici.ubicacion[0]).toBe(-26.812386);
//         expect(targetBici.ubicacion[1]).toBe(-65.263015);

//         targetBici = Bicicleta.findById(2);
//         expect(targetBici.id).toBe(2);
//         expect(targetBici.color).toBe("Blanco");
//         expect(targetBici.modelo).toBe("Montaña");
//         expect(targetBici.ubicacion[0]).toBe(-26.812386);
//         expect(targetBici.ubicacion[1]).toBe(-65.263050);
//     });
// });

// describe('Bicicleta.removeById', () => {
//     it('debe borrar una bici con id 1', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0)
//         var aBici1 = new Bicicleta(1,"Verde", "Urbana");

//         Bicicleta.add(aBici1);

//         Bicicleta.removeById(1);

//         bici = Bicicleta.allBicis.find(x => x.id == 1);
//         expect(bici).toBe(undefined);
//     });
// });

