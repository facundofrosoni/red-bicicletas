var Bicicleta = require('../../models/bicicleta.js');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis((err,bicis)=>{
        if(err) console.log(err);
        res.status(200).json({
            bicicletas: bicis,
        });
    });
    // res.status(200).json({
    //     bicicletas: Bicicleta.allBicis
    // });
}

exports.bicicleta_create = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_update = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.update(req.body._id, bici, ()=>{
        res.status(200).json({
            bicicleta: bici
        });
    })
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.code, ()=>{    
        res.status(200).send();
    });
    // res.status(200).json({
    //     bicicletas: Bicicleta.allBicis
    // });
}

