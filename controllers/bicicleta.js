var Bicicleta = require('../models/bicicleta.js');

exports.bicicleta_list = function (req, res){
    // res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
    Bicicleta.allBicis((err,bicis)=>{
        if(err) console.log(err);
        res.render('bicicletas/index', {bicis: bicis});
    });
}

exports.bicicleta_create_get = function (req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res){
    // var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    // bici.ubicacion = [req.body.lat, req.body.lng];
    // Bicicleta.add(bici);
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bici);
    
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (req, res){
    Bicicleta.findById(req.params._id, function(err, bici){
        res.render('bicicletas/update', {bici: bici});
    });
}

exports.bicicleta_update_post = function (req, res){
    Bicicleta.findById(req.params._id, function(err, bici){
        if(err) console.log(err);
        else {
            bici.code=req.body.code;
            bici.color=req.body.color;
            bici.modelo=req.body.modelo;
            bici.ubicacion=[req.body.lat, req.body.lng];
            bici.save();
        }
    });
    
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req, res){
    Bicicleta.removeById(req.body._id, ()=>{
        res.redirect('/bicicletas');
    });
}
