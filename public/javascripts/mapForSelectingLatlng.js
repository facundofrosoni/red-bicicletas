var map = L.map('main_map').setView([-26.812386, -65.263015], 100);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    data: '{"email": "test@gmail.com", "password": "asdf1234"}',
    dataType: "json",
    url: "../api/auth/authenticate",
    headers: {'content-type': 'application/json'},
    type: 'POST',
    success: function(result){
        var token = result.data.token;
        $.ajax({
            dataType: "json",
            url: "../api/bicicletas",
            headers: {'x-access-token': token},
            success: function(result){
                result.bicicletas.forEach(function(bici) {
                    L.marker(bici.ubicacion, {tittle: bici.code}).addTo(map).bindPopup("bicicleta número: " + bici.code).openPopup();
                });
            }
        })
    }
}).fail( function( jqXHR, textStatus, errorThrown ) {
    console.log(textStatus);
    console.log(errorThrown);
});

var greenIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var tempMarker = L.marker([0,0], {icon: greenIcon});

var latitud = document.getElementById('lat');
var longitud = document.getElementById('lng');

map.on('click', function(ev){
    var latlng = map.mouseEventToLatLng(ev.originalEvent);
    console.log(latlng.lat + ', ' + latlng.lng);
    tempMarker.setLatLng(latlng);
    tempMarker.addTo(map).bindPopup("ubicación seleccionada").openPopup();
    latitud.value = latlng.lat;
    longitud.value = latlng.lng;
});