var map = L.map('main_map').setView([-26.812386, -65.263015], 100);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
//     accessToken: 'pk.eyJ1IjoiZXhlbnYiLCJhIjoiY2tpd2FhNHZsMGhscTJzcDNvOGwxZ2QzZCJ9.6bd-DlcEoc77tbdMpmN6xA'
// }).addTo(map);

$.ajax({
    data: '{"email": "test@gmail.com", "password": "asdf1234"}',
    dataType: "json",
    url: "api/auth/authenticate",
    headers: {'content-type': 'application/json'},
    type: 'POST',
    success: function(result){
        var token = result.data.token;
        $.ajax({
            dataType: "json",
            url: "api/bicicletas",
            headers: {'x-access-token': token},
            success: function(result){
                result.bicicletas.forEach(function(bici) {
                    L.marker(bici.ubicacion, {tittle: bici.code}).addTo(map).bindPopup("bicicleta número: " + bici.code).openPopup();
                });
            }
        })
    }
}).fail( function( jqXHR, textStatus, errorThrown ) {
    console.log(textStatus);
    console.log(errorThrown);
});


// $.ajax({
//     dataType: "json",
//     url: "api/bicicletas",
//     headers: {'x-access-token': token},
//     success: function(result){
//         console.log("result: " + result);
//         result.bicicletas.forEach(function(bici) {
//             L.marker(bici.ubicacion, {tittle: bici.id}).addTo(map);
//         });
//     }
// })


// var request = require('request');
// var server = require('../../bin/www');

// var token;

// request.post({
//     headers: {'content-type':'application/json'},
//     url: 'http://localhost:5000/api/auth/authenticate',
//     body: {
//         "email": "test@gmail.com",
//         "password": "asdf1234"
//     }
// }, function (error, response, body) {
//     token = JSON.parse(body).data.token;
//     console.log(token);
// });

// console.log(token);